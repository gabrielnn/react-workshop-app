import React, { Component } from 'react';
import logo from './logo.svg';
import AppHeader from './components/header/header.component';
import AppContent from './components/content/content.component';
import AppFooter from './components/footer/footer.component';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AppHeader />
        <AppContent />
        <AppFooter />
      </div>
    );
  }
}

export default App;
