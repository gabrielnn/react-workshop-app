import React from 'react';
import SideBar from './content-sidebar/sidebar.component';
import Blog from './content-blog/blog.component';

export default function AppContent(props) {
  return (
    <main role="main" className="container">
      <div className="row">
        <Blog />
        <SideBar />
      </div>
    </main>
  );
}
