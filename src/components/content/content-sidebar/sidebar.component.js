import React from 'react';
import SideBarAbout from './sidebar-about.component';
import SideBarArchives from './sidebar-archives.component';
import SideBarElsewhere from './sidebar-elsewhere.component';

export default function Sidebar(props) {
  return (
    <aside className="col-sm-3 ml-sm-auto blog-sidebar">
      <SideBarAbout />
      <SideBarArchives />
      <SideBarElsewhere />
    </aside>
  );
}
