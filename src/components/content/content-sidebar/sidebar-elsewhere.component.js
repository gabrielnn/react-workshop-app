import React from 'react';

export default function SideBarElsewhere(props) {
  return (
    <div className="sidebar-module">
      <h4>Elsewhere</h4>
      <ol className="list-unstyled">
        <li>
          <a href="#">GitHub</a>
        </li>
        <li>
          <a href="#">Twitter</a>
        </li>
        <li>
          <a href="#">Facebook</a>
        </li>
      </ol>
    </div>
  );
}
