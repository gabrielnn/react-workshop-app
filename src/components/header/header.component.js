import React from 'react';
import HeaderNav from './header-nav.component';
import HeaderContent from './header-content.component';

export default function AppHeader(props) {
  return (
    <header>
      <HeaderNav />
      <HeaderContent />
    </header>
  );
}
